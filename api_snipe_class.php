<?php
Class Snipe_Apis
{
	public function __construct()
	{
		$this->client 	= new GuzzleHttp\Client();
		$this->debug  	= true;
		$this->snipe 	= 'http://snipe01/';
		$this->jss 	  	= 'https://jamfp01:8443/';
		$this->apikey    = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjhjZmE1ZDY0OTNiZTRiZjIxNThmNzNkMGQ4YzE3MmY4MTIzMjdmNDgwMzkwOTkyMmMyZTdhNTc0YmZiZDZlNTA1Yzk1OTM2ZGI5MTUxYWQzIn0.eyJhdWQiOiIzIiwianRpIjoiOGNmYTVkNjQ5M2JlNGJmMjE1OGY3M2QwZDhjMTcyZjgxMjMyN2Y0ODAzOTA5OTIyYzJlN2E1NzRiZmJkNmU1MDVjOTU5MzZkYjkxNTFhZDMiLCJpYXQiOjE1MzAwNzU4ODcsIm5iZiI6MTUzMDA3NTg4NywiZXhwIjoxNTYxNjExODg3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.KRGYmc5s53owPHtZ9rL85elqBNeo3rSGsQgavXqdjUxSH77tIJAdoyn2Ld2A6iKDn0kqcBUX8fBMsgbJNPyBg08PNzb1cN5NsA52dQD5Sc9GNOvQ40t8mpVuqKNXwuOd58aCdu4PgqLK692Yf0vcGah2IbnhvjR5aw480WDh6omY09PqSk8Vikyd2DX9Qq9vZSAvoNMt8LHI0JXCbAE9VTiE0l-PIYm6eg-XEWP4w6So5EFi5XGTAsRYJo_meWU5g2yvfdgqPblBeSlyD3K2wV6DPi2YkpkUnbGAF299axvybCLa5UvgmkKMrZWNswNiH6JWwAttBbJEF7_xbJVtbVb6q7apucobvbNFWf27QaTzVWtcwjNdyIIUo5YnUPqejMenV-MtXlEbwWNVpCFkS9S8rMBMMmOFuROwX3LzO4pTzY2xQ6MErZjBK524lC79j8vRQq4MGmHWIyuRB2jvAV3Jd4IPa8owcb45BS0t42R8fQ3_yY6J8U72W-ZqYidclqcHBtVJoRxGBCvCGLTCzelyaYlUGLc2S-D2MGzMLK4dDXGiyH2UY__cvAe4CoCsSilgemy_43vb8LZw_TK2WSQ2opFeBiQvDFevNyPgD2744sds311umU-sJ2o_bkGWFaJYybLD76vhY6mK5gI3RBHIKBIHWsdCn0pPggcUGvw";

	}

	
	/**
	 * Snipereq
	 * 
	 * Performs the REST requests agaisnt the Snipe IT Server 
	 * 
	 * @param 	string	$uri	URI of the Snipe Server
	 * @param	string	$method	get/post/put/delete
	 * @param	mixed	$body 	The actual request
	 * @param   bool	$json_decode	Optinally can decode it from json
	 */
	public function Snipereq ($uri, $method='get', $body=false, $json_decode=true)
	{
				
		$rs['headers']	= [
						'User-Agent'	=> 'jamfit/0.1',
						'Accept'    	=> 'application/json',
						'Authorization' => 'Bearer '.$this->apikey,
								];
		if($body)
		{
			$rs['form_params'] = $body;
		}
		else
		{
			$rs['form_params'] = [];
		}
		$rs['verify'] = false;

		$req = $this->client->request($method, $this->snipe.'api/v1/'.$uri, $rs);
		
		$res = $req->getBody()->getContents();
		$result = $json_decode ? json_decode($res, true) : $res;
		return $result;
	}

	/**
	 * Snipe_ids
	 * 
	 * Grabs all of the IDs in Snipe for a given path
	 * 
	 * @param 	string	$uri 	The URI path
	 * @return	string	
	 * 
	*/
	public function Snipe_ids ($uri)
	{
		$models = $this->Snipereq($uri);
		$ids    = array();
		foreach($models['rows'] as $model)
		{
			$ids[] = $c[0]['id'];
		}
		return $ids;
	}

	/**
	 * Snipe_add
	 * 
	 * Adds a computer to Snipe.  If the model doesn't exist, make it.  Set the staus to "2" which is "deployed"
	 * Return the Snipe ID of the computer.
	 * $a is in the format
	 * $a['computer']['general]['name'];
	 * $a['computer']['general']['id']
	 * $a['computer']['general']['serial_number'];
	 * $a['computer']['hardware']['model'];
	 * 
	 * @param array	$a	Array of the computer to add
	 * @return void
	 */
	public function Snipe_add ($a)
	{
		$p['asset_tag'] = $a['computer']['general']['name'];
		$p['model_id']	= $this->Snipe_get_model($a['computer']['hardware']['model'], $a['computer']['hardware']['model_identifier']);
		$p['serial']	= $a['computer']['general']['serial_number'];
		$p['notes']		= 'View JSS record at ' .$this->jss. 'computers.html?id='.$a['computer']['general']['id']. '&o=r';
		$p['status_id'] = 2; //Deployed
		$ass = $this->Snipereq('hardware', 'post', $p);
	}


	/**
	 * Snipe_get_cat
	 * 
	 * Attempt to figure out the category from the Apple model name.  Use "model_clues"
	 * to figure out what category of device it is
	 * 
	 * @param 	string 	$name
	 * @return 	mixed 	Either an array of the cat & id or false
	 */
	public function Snipe_get_cat($name)
	{
		$model_clues['macbook'] = 'Laptops';
		$model_clues['imac']	= 'Desktops';
		$model_clues['macpro']	= 'Desktops';
		$model_clues['pad']		= 'Tablets';
		$model_clues['phone']	= 'Mobile';

		foreach($model_clues as $clue => $cat)
		{
			if(false !== stripos($name, $clue)) //need to use !== as it will return 0 if the word is at the first position
			{
				$category = $cat;
				break;
			}
			$category = 'Default';
		}
		$scat = $this->Snipereq('categories?search='.urlencode($category));
		if(0 == $scat['total'])
		{
			return false;
		}
		else
		{
			return $scat['rows'][0]['id'];
		}
		//return $scat;

	}

	private function Snipe_create_model($name, $number=false)
	{
		$p['name'] 			    = $name;
		$p['category_id'] 		= $this->Snipe_get_cat($name);
		$p['manufacturer_id'] 	= $this->Snipe_get_manufacturer('Apple');
		$p['model_number'] 	    = $number;
		// Create the model;
		$r 	= $this->Snipereq('models', 'post', $p);
		if('success' === $r['status'])
		{
			return $r['payload']['id'];
		}
		else
		{
			print "Could not create model: "; 
			print_r($r);
			exit;
		}
	}

	/**
	 * [Snipe_get_model description]
	 * @param string  $name  	Model Name
	 * @param mixed $number 	Model number (optional)
	 *
	 * @return  array retuns the model ID and the name
	 * Gets the model ID from Snipe.  If the model doens't exist, it creates it.
	 * 
	 */
	public function Snipe_get_model($name, $number=false)
	{
		$model = $this->Snipereq('models?search='.urlencode($name));
		if(0 == $model['total'])
		{
			$ret = $this->Snipe_create_model($name, $number);
		}
		else
		{
			$ret = $model['rows'][0]['id'];
		}
		return $ret;
		//return $scat;
	}

	public function Snipe_get_manufacturer($name='Apple')
	{
		$man = $this->Snipereq('manufacturers?search='.urlencode($name));
		if(0 == $man['total'])
		{
			$ret = false;
		}
		else
		{
			$ret = $man['rows'][0]['id'];
		}
		return $ret;
		//return $scat;
	}	


}







