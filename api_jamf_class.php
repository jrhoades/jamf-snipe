<?php
Class Jamf_apis
{
	public function __construct()
	{
		$this->client 	= new GuzzleHttp\Client();
		$this->debug  	= true;
		$this->jss_api  = 'https://jamfp01:8443/JSSResource';
		$this->jss_user = 'snipeapi';
		$this->jss_pass	= 'jamf123!';
	}

	/**
	 * Jssreq
	 * 
	 * This method will perform a REST request agaisnt the Jamf Pro server
	 * 
	 * @param	string	$uri 	URI of the JSS
	 * @param	string	$method	get/post/put/delete
	 * @param	mixed	$body	the actual request
	 * @param	string	$enc	application/json or xml
	 * @param   bool	$json_decode	Optinally can decode it from json
	 * @todo	error checking!
	 * @return 	string
	 * 
	 */
	public function Jssreq ($uri, $method='get', $body=false, $enc='application/json', $json_decode=true)
	{
		$rs['auth'] 	= [$this->jss_user, $this->jss_pass];
		$rs['headers'] 	= [
					'User-Agent'=> 'jamfit/0.1',
					'Accept'    => $enc,
					];
		if($body && $enc == 'application/json')
		{
			$rs['form_params'] = $body;
		}
		elseif($body)
		{
			$rs['body']	= $body;
		}
		else
		{
			$rs['form_params'] = [];
		}					
		$rs['verify'] 	= false;					
		$req = $this->client->request($method, $this->jss_api.'/'.$uri, $rs);
		
		$ret = $req->getBody()->getContents();
		//If needed json decode it
		$result = $json_decode ? json_decode($ret, true) : $ret;
		return $result;
	}

	/**
	 * jss_update_group
	 * 
	 * This updates a fiven computer group with a given computer ID
	 * 
	 * @param	string	$group	The ID of the Jamf Computer Group
	 * @param	string	$computer_id	The ID of the computer
	 * @todo	Device groups
	 * @return	string
	 */
	public function jss_update_group($group, $computer_id)
	{
		$group_id 	= $this->jss_get_group_id($group);
		$body 		= "<computer_group><computer_additions><computer><id>".$computer_id."</id></computer></computer_additions></computer_group>";
		$r          = $this->Jssreq('computergroups/id/'.$group_id, 'put', $body, 'application/xml', false);
	}

	/**
	 * jss_get_group_id
	 * 
	 * Gets the name of a given Jamf computer group
	 * 
	 * @param 	string	$name	The name of the group
	 * @return	mixed	False if it's not there or the name of the group
	 */
	private function jss_get_group_id($name)
	{
		$g = $this->Jssreq('computergroups/name/'.$name, 'get');
		if(0 !== count($g['computer_group']))
		{
			return $g['computer_group']['id'];
		}
		else
		{
			return false;
		}
	}
}
