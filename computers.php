#!/usr/bin/php
<?php
// Grabs the computers from Jamf

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

require 'api_jamf_class.php';
require 'api_snipe_class.php';
$j = new Jamf_apis();
$s = new Snipe_apis();

$cids 		= array();
$cseraials  = array();

/**
 * Get all of the computers not in the "not sniped computer group"
 */
$computers 	= $j->Jssreq('computergroups/name/notsniped', 'get');
if(0 === count($computers['computer_group']['computers']))
{
	print "No Computers to add\n";
	exit;
}
/**
 * Get an array of just the IDs from the computer group as this will strip all of the
 * surplus info
 */
foreach($computers['computer_group']['computers'] as $c)
{
	$cids[] = $c['id'];
}
/**
 * Grab the Jamf ID of each computer form the group
 * Assign it to an array in the format:
 * $cserials[serial number] = ID
 */
foreach ($cids as $id)
{
	$comp = $j->Jssreq('computers/id/'.$id, 'get');
	$cserials[$comp['computer']['general']['serial_number']] = $id;;
}

/*
foreach($ass['rows'] as $a)
{
	if('Apple' !== $a['manufacturer']['name']){continue;} //Ignore anything non apple
	$aserials[$a['serial']] = $a['id'];
}
*/

foreach($cserials as $serial => $id)
{
	if(!isset($aserials[$serial]))
	{
		print "Adding Jamf ID".$id;
		$comp = $j->Jssreq('computers/id/'.$id, 'get');
		$s->Snipe_add($comp);
		$j->jss_update_group('sniped', 1); //Add to static group
	}
	else
	{
		print "None to add\n";
	}
}



